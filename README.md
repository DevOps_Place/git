# GIT

![git](/uploads/202d105fcd0fa4314fb14da721ed2a09/git.png)

## Table of content 

- [What is Version Control System ?](#wa)

- [GIT ](#kc)

- [DVCS Terminologies](#INST) <br>

- [Git keywords](#yaml) <br>

- [Installation of Git Client](#pods)<br>

- [Customize Git Environment](#ad)<br>

- [Install gitlab for remote repository](#gitlab)

- [Start a new git repository](#deploy)

- [Clone operation](#clone)

- [Git - Perform Changes!](#CH)

- [Git - Review Changes](#dt)

- [Git - Commit Changes](#ing)

- [Git - PULL](#bb)

- [Git - fetch ](#cc)

- [Git - Stash Operation ](#ee)

- [Git - Remove Operation ](#ww)

- [Revert Uncommitted Changes ](#rr)

- [Revert Uncommitted Changes ](#qq)

- [Move HEAD Pointer with Git Reset ](#tt)

- [Git - Tag Operation ](#kk)

- [Git - Managing Branches ](#ll)


<a name="wa"></a>

## What is Version Control System ?

Version Control System (VCS) is a software that helps software developers to work together and maintain a complete history of their work.

**Listed below are the functions of a VCS :**

- Allows developers to work simultaneously.
- Does not allow overwriting each other’s changes.
- Maintains a history of every version.

**Following are the types of VCS:**

- Centralized version control system (CVCS).

- Distributed/Decentralized version control system (DVCS).


Centralized version control system (CVCS) uses a central server to store all files and enables team collaboration. But the major drawback of CVCS is its single point of failure, i.e., failure of the central server. Unfortunately, if the central server goes down for an hour, then during that hour, no one can collaborate at all. And even in a worst case, if the disk of the central server gets corrupted and proper backup has not been taken, then you will lose the entire history of the project. Here, distributed version control system (DVCS) comes into picture.

DVCS clients not only check out the latest snapshot of the directory but they also fully mirror the repository. If the server goes down, then the repository from any client can be copied back to the server to restore it. Every checkout is a full backup of the repository. Git does not rely on the central server and that is why you can perform many operations when you are offline. You can commit changes, create branches, view logs, and perform other operations when you are offline. You require network connection only to publish your changes and take the latest changes.

> Git is a distributed version control system (DVCS), or peer-to-peer version control system, as opposed to centralized systems like Subversion.

<a name="GIT"></a>

### GIT 

Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

**Advantages of Git**

1. Free and open source
2. Fast and small

> As most of the operations are performed locally, it gives a huge benefit in terms of speed. Git does not rely on the central server; that is why, there is no need to interact with the remote server for every operation. The core part of Git is written in C, which avoids runtime overheads associated with other high-level languages. Though Git mirrors entire repository, the size of the data on the client side is small. This illustrates the efficiency of Git at compressing and storing data on the client side.

3. Implicit backup

>  Data present on any client side mirrors the repository, hence it can be used in the event of a crash or disk corruption.

4. Security

>Git uses a common cryptographic hash function called secure hash function (SHA1), to name and identify objects within its database it is impossible to change file, date, and commit message and any other data from the Git database without knowing Git.

5. No need of powerful hardware

6. Easier branching

> CVCS uses cheap copy mechanism, If we create a new branch, it will copy all the codes to the new branch . But branch management with Git is very simple. It takes only a few seconds to create, delete, and merge branches.


<a name="INST"></a>

### DVCS Terminologies

Git give you a local repository place that you can modify,add,delete and change pricately before you decide to publish it or share it with others not like CVCS tools that require the devloper to change directly to the repo .

Git consist of 3 stages as belwo :

![git](/uploads/6a83f7d4a556d01243156d646f85296e/git.png)

_Working directory:_

The Working directory  is the area where you are currently working. It is where your files live. This area is also known as the “untracked” area of git. Any changes to files will be marked and seen in the Working Tree. Here if you make changes and do not explicitly save them to git, you will lose the changes made to your files. This loss of changes occurs because git is not aware of the files or changes in the Working directory  until you tell it to pay attention to them. If you make changes to files in your working directory git will recognize that they are modified, but until you tell git “Hey pay attention to these files,” it won’t save anything that goes on in them.

_Staging Area:_

The Staging Area is when git starts tracking and saving changes that occur in files. These saved changes reflect in the .git directory. That is about it when it comes to the Staging Area. You tell git that I want to track these specific files, then git says okay and moves them from you Working Tree to the Staging Area and says “Cool, I know about this file in its entirety.” However, if you make any more additional changes after adding a file to the Staging Area, git will not know about those specific changes until you tell it to see them. You explicitly have to tell git to notice the edits in your files.

_Local Repository:_

The Local Repository is everything in your .git directory. Mainly what you will see in your Local Repository are all of your checkpoints or commits. It is the area that saves everything (so don’t delete it). That’s it.




<a name="yaml"></a>

### Git keywords

_Blobs_

Blob is an abbreviation for “binary large object”. When we git add a file such as example_file.txt, git creates a blob object containing the contents of the file. Blobs are therefore the git object type for storing files , the files store in git database not by named but with SHA1 address .

_Trees_

Tree is an object, which represents a directory. It holds blobs as well as other sub-directories. A tree is a binary file that stores references to blobs and trees which are also named as SHA1 hash of the tree object.

_HEAD_

HEAD is a pointer, which always points to the latest commit in the branch. Whenever you make a commit, HEAD is updated with the latest commit. The heads of the branches are stored in .git/refs/heads/ directory.

_Revision_
Revision represents the version of the source code. Revisions in Git are represented by commits. These commits are identified by SHA1 secure hashes.

_URL_
URL represents the location of the Git repository. Git URL is stored in config file.


<a name="pods"></a>

### Installation of Git Client


**Debian-based linux systems**

```bash
sudo apt update
sudo apt upgrade
sudo apt install git
```
**Red Hat-based linux systems**

```bash
sudo yum upgrade
sudo yum install git
```

<a name="ad"></a>

###  Customize Git Environment

Git provides the git config tool, which allows you to set configuration variables. Git stores all global configurations in .gitconfig file, which is located in your home directory. To set these configuration values as global, add the --global option, and if you omit --global option, then your configurations are specific for the current Git repository.

_Setting username_

```bash
git config --global user.name "Yazeed AlSmadi"
```

_Setting Email_

```bash
git config --global user.email "Yazeed.Smadi@ehs.com.jo"
```

_Color highlighting_

```bash
git config --global color.ui true

git config --global color.status auto

git config --global color.branch auto
```

_Setting default editor_

```bash
git config --global core.editor gedit
```

_Setting default merge tool_

```bash
git config --global merge.tool vimdiff
```

_Listing Git settings_

```bash 
git config --list
```


<a name="gitlab"></a>

### Install gitlab for remote repository:

GitLab is a free and paid self-hosted web-based repository manager for the team collaboration of development tools. It is used to create, edit new projects, merge finished code into the existing projects, and also as an intermediate between the server and the user. In this tutorial we will cover how to install GitLab on Redha Hat distro .

Install all pre-requests packages with the apt command below.

```bash
yum -y install curl policycoreutils openssh-server openssh-clients postfix
```

Start Postfix and sshd :

``` bash 
systemctl start sshd
systemctl start postfix
systemctl enable sshd
systemctl enable postfix
```

Add GitLab repository with the curl command.

```bash
curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo 
```

And then install GitLab CE Community Edition with the apt command.

```bash
yum  install gitlab-ce
```

Now we should configure the URL that will be used to access our GitLab server. 

```bash
cd /etc/gitlab
vim gitlab.rb
```

```yml
external_url 'http://yazeed_repo.co'
```

Generate SSL Let's encrypt and DHPARAM certificate

For the basic layer of security, we will be using the SSL for our GitLab site. We will use free SSL certificate from Letsencrypt and generate DHPARAM certificate to add an extra security layer.

Install Letsencrypt tool on RHEL 7 with yum command below.

```bash
yum -y install letsencrypt
```

After the installation is complete, generate new SSL certificate letsencrypt with the command below.

```bash
letsencrypt certonly --standalone -d yazeed.co
```

Next, create new 'ssl' directory under the GitLab configuration directory '/etc/gitlab/'.

```bash
mkdir -p /etc/gitlab/ssl/
```

Now generate the DHPARAM certificate pem file using OpenSSL. The bigger bit is more secure.

```bash
sudo openssl dhparam -out /etc/gitlab/ssl/dhparams.pem 2048
```

And after the DHPARAM certificate is generated, change the permission of the certificate file to 600.

```bash
chmod 600 /etc/gitlab/ssl/*
```

Enable Nginx HTTPS for GitLab

```bash
cd /etc/gitlab/
vim gitlab.rb
```
And change HTTP to HTTPS on the external_url line.

```bash
external_url 'https://gitlab.hakase-labs.co'
```
Then paste the following configuration under the 'external_url' line configuration.

```yml
nginx['redirect_http_to_https'] = true
nginx['ssl_certificate'] = "/etc/letsencrypt/live/yazeed.co/fullchain.pem"
nginx['ssl_certificate_key'] = "/etc/letsencrypt/live/yazeed.co/privkey.pem"
nginx['ssl_dhparam'] = "/etc/gitlab/ssl/dhparams.pem"
```
Finally, apply the GitLab configuration using the following command.

```bash
gitlab-ctl reconfigure
```
<a name="deploy"></a>

### Start a new git repository

_A new repo from scratch_

- Create a directory to contain the project.
- Go into the new directory.
- Type 
  ```bash
  git init
  ```
- Write some code.

- Type
```bash
git add
```
- Type
```bash
 git commit
 ```

_A new repo from an existing project_

Say you’ve got an existing project that you want to start tracking with git.

- Go into the directory containing the project.

- Type 
  ```bash
  git init
  ```
- Write some code.

- Type
```bash
git add
```
- Type
```bash
 git commit
 ```

_ Connect it to remote repository _
```bash
cd existing_folder
```
```bash
git init
```
```bash
git remote add origin http://yazeed.co/root/test.git
```
```bash
git add .
```
```bash
git commit -m "Initial commit"
```
```bash
git push -u origin master
```

<a name="clone"></a>

### Clone operation

The Clone operation creates an instance of the remote repository. you can clone your repo via http or https or sshd

![git](/uploads/58bb12899f27a25275c5744641be0c23/git.png)


to clone it via ssh you have to modify the member setting and add the ssh key to allow the user authunticate to remote repo and grant the needed access 
you have to add you pupblic key as below :

```
cat ~/.ssh/id_pub 
```

![git](/uploads/1bb13d9e53fcc77dde34f969cd5d8145/git.png)


<a name="CH"></a>

### Git - Perform Changes! 

The repo that we alread cloned name is test include only one  file named as test 

```bash
root@master-node:/opt/test# ll
total 12
drwxr-xr-x  3 root root 4096 Apr 10 01:00 ./
drwxr-xr-x 44 root root 4096 Apr 10 01:00 ../
drwxr-xr-x  8 root root 4096 Apr 10 01:00 .git/
-rw-r--r--  1 root root    0 Apr 10 01:00 test
```

We will rename the file test to CODE1 (rename file is also considered as a change )

```bash
root@master-node:/opt/test# git mv test code1
```
To check the status of the change 

```bash
root@master-node:/opt/test# git status 
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	renamed:    test -> code1
```
Move the change to staging area 

```bash
root@master-node:/opt/test# git add . 
```
Save the changes to the local repository

```bash
root@master-node:/opt/test# git commit -m "NAME OF FILE CHANGED"
[master 2edd623] NAME OF FILE CHANGED
 1 file changed, 0 insertions(+), 0 deletions(-)
 rename test => code1 (100%)
root@master-node:/opt/test# git status 
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```

<a name="dt"></a>

### Git - Review Changes!

To check the logs of all commits that accomplished to my repo 

```bash 
git log 
```
```yaml
commit 2edd62336498871f13d74d0d6c5764a892ddbcc4 (HEAD -> master)
Author: Yazeed AlSmadi <Yazeed.Smadi@ehs.com.jo>
Date:   Sat Apr 10 01:06:26 2021 +0300

    NAME OF FILE CHANGED

commit cb2b20f2e7d2464f586dfe400267270d32295dd9 (origin/master, origin/HEAD)
Author: YS <yazeed.smadi@ehs.com.jo>
Date:   Sat Apr 10 00:47:41 2021 +0300

    Initial commit
```

To trace a specific commit and get detailed information about the commit 

```
git show <commit hash>
```

```yaml
root@master-node:/opt/test# git show 2edd62336498871f13d74d0d6c5764a892ddbcc4
commit 2edd62336498871f13d74d0d6c5764a892ddbcc4 (HEAD -> master)
Author: Yazeed AlSmadi <Yazeed.Smadi@ehs.com.jo>
Date:   Sat Apr 10 01:06:26 2021 +0300

    NAME OF FILE CHANGED

diff --git a/test b/code1
similarity index 100%
rename from test
rename to code1
```
<a name="dt"></a>

### Git - Commit Changes

The amend operation changes the last commit including your commit message; it creates a new commit ID.

```git
git commit --amend -m ''
```

it will replace last commit messege with new commit messege with new ID

<a name="bb"></a>

### Git - PULL

The git pull command is used to fetch and download content from a remote repository and immediately update the local repository to match that content. Merging remote upstream changes into your local repository is a common task in Git-based collaboration work flows.

```git
git pull 
```

<a name="cc"></a>

### Git - fetch

hen you fetch, Git gathers any commits from the target branch that do not exist in your current branch and stores them in your local repository. However, it does not merge them with your current branch. This is particularly useful if you need to keep your repository up to date, but are working on something that might break if you update your files. To integrate the commits into your master branch, you use merge.

```git
git fetch 
```

```yml
root@master-node:/opt/test# git branch -r
  origin/HEAD -> origin/master
  origin/master
```

The above command will show you that there will be 2 copies of remote repo you can switch to   origin/master to see the updated data that we have from fetch command .

<a name="ee"></a>

### Git - Stash Operation

Use git stash when you want to record the current state of the working directory and the index, but want to go back to a clean working directory.

```git
git stash 
```

It will record the modified files from staging and giv you a clean local repo .


We can view a list of stashed changes by using the git stash list command.

```git
git stash list
```

git stash pop command, to remove the changes from the stack and place them in the current working directory

```git
git stash pop
```
<a name="ww"></a>

## Git - Remove Operation 

The "rm" command helps you to remove files from a Git repository. It allows you to not only delete a file from the repository, but also - if you wish - from the filesystem.

Deleting a file from the filesystem can of course easily be done in many other applications, e.g. a text editor, IDE or file browser. But deleting the file from the actual Git repository is a separate task, for which git rm was made.

```bash
git rm <FILE-NAME>
```

 When doing git rm instead of rm, Git will block the removal if there is a discrepancy between the HEAD version of a file and the staging index or working tree version. This block is a safety mechanism to prevent removal of in-progress changes.

To restore file that deleted by git rm 

```bash
git restore --staged <file>
```
To restore file that deleted by  rm 

```bash
git restore  <file>
```

<a name="rr"></a>

## Revert Uncommitted Changes

every VCS provides a feature to fix mistakes until a certain point. Git provides a feature that we can use to undo the modifications that have been made to the local repository.

Suppose the user accidentally does some changes to his local repository and then wants to undo these changes. In such cases, the revert operation plays an important role.

To handle this situation, we can use the git checkout command. We can use this command to revert the contents of a file.

```git
git checkout <file name>
```

<a name="qq"></a>

## Revert committed Changes

In Git, there is one HEAD pointer that always points to the latest commit. If you want to undo a change from the staged area, then you can use the git checkout command, but with the checkout command, you have to provide an additional parameter, i.e., the HEAD pointer. The additional commit pointer parameter instructs the git checkout command to reset the working tree and also to remove the staged changes.

```git
git checkout HEAD -- <file name>
```


<a name="tt"></a>

## Move HEAD Pointer with Git Reset

After doing few changes, you may decide to remove these changes. The Git reset command is used to reset or revert changes. We can perform three different types of reset operations.

Below diagram shows the pictorial representation of Git reset command.

![git](/uploads/f2d3a5e37b61547f7340d4969769a4ef/git.png)

_Soft_
Each branch has a HEAD pointer, which points to the latest commit. If we use Git reset command with --soft option followed by commit ID, then it will reset the HEAD pointer only without changing the file in working directory it will put the old file in staging area .

```bash
git reset --soft <commit id>
```

_mixed_

Git reset with --mixed option reverts those changes from the staging area that have not been committed yet. It reverts the changes from the staging area only. The actual changes made to the working copy of the file are unaffected. The default Git reset is equivalent to the git reset -- mixed it will bring the cahnges to the working directory and wll not affect the content of file .

```bash 
git reset --mixed <commit id>
```

_hard_
If you use --hard option with the Git reset command, it will clear the staging area; it will reset the HEAD pointer to the latest commit of the specific commit ID and delete the local file changes too.

```bash
git reset --hard <commit id>
```
<a name="kk"></a>

## Git - Tag Operation

Tag operation allows giving meaningful names to a specific version in the repository. 

```bash
git tag -a 'Release_1_0' -m 'Tagged basic string operation code' HEAD
```

You can give a tag for a specifc commit that you have 

```bash 
git tag -a 'Release_1_0' -m 'Tagged basic string operation code' <commitID>
```

You can list all tags you have 

```bash
git tag -l 
```

You can delete the tag by appling the below 

```bash
git tag -d <TAG-NAME>
```

<a name="ll"></a>

## Git - Managing Branches

Branch operation allows creating another line of development. We can use this operation to fork off the development process into two different directions. For example, we released a product for 6.0 version and we might want to create a branch so that the development of 7.0 features can be kept separate from 6.0 bug fixes.

- Create Branch

```bash
git branch new_branch
```

![git](/uploads/3f0d1039a2abebed07b469e16a053b9c/git.png)

- List branches 

```bash
git branch -l
```

- Switch between Branches

```bash
git checkout <branch name>
```

- Shortcut to Create and Switch Branch

```bash
 checkout -b  <branch name>
 ```

 - Delete a Branch

 ```bash
git branch -D <branch name>
```

- Rename a Branch

```bash
git branch -m new_branch old_branch
```

- Merge Two Branches

```bash
git merge <branch want to merge with>
```

![git](/uploads/0014260058f976c36ea431a018c8b9d2/git.png)
![git](/uploads/d36c94807edd9c815f44db0ef2e9078c/git.png)

Git rebase and merge both integrate changes from one branch into another. Where they differ is how it's done. Git rebase moves a feature branch into a master. Git merge adds a new commit, preserving the history.

![git](/uploads/d0b728d2c36472905f2b01a119cf449c/git.png)


